class Api::V1::ItemController < ApplicationController

    skip_before_action :verify_authenticity_token
    def create
        begin
            Item.create(create_item params)
            render json: "Successfully added to cart", status: 200
        rescue
            render json: "request time out please try after sometime", status: 500
        end
    end

    def index
        render json: show_items, status: 200
    end

    def show_items
        items = []
        Item.all.each do |item|
            items << show_item(item)
        end
        items
    end

    def destroy
        Item.delete(params[:id])
    end

    def show_item params
        item = {}
        item[:name] = params[:name]
        item[:description] = params[:description]
        item[:price] = params[:price].to_f
        item[:product_id] = params[:product_id].to_i
        item[:id] = params[:id].to_i
        item
    end

    def create_item params
        item = {}
        item[:name] = params[:name]
        item[:description] = params[:description]
        item[:price] = params[:price].to_f
        item[:product_id] = params[:id].to_i
        item
    end

    def checkout
        payload = show_items
        #call any API to handle payments and respond accordingly and emptly cart
        render json: "Total price: #{total_price(payload)}", status: 200
        empty_cart
    end

    def empty_cart
        Item.delete_all
    end

    def total_price payload
        tp = 0
        payload.each do |p|
            tp += p[:price].to_f
        end
        tp
    end
end
