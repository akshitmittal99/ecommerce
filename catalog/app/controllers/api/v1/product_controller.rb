class Api::V1::ProductController < ApplicationController
    
    skip_before_action :verify_authenticity_token
    def index
        render json: map_products, status: 200
    end

    def show
        render json: map_product(Product.find_by(id: params[:id])), status: 200
    end

    def create
        begin
            Product.create(get_product(params))
            render json: "Successfully created", status: 200
        rescue
            render json: {message: "invalid Input"}, status: 500
        end
    end

    def destroy
        Product.delete(params[:id])
        render json: "deleted", status: 200
    end

    def get_product params
        name = params[:name]
        description = params[:description]
        price = params[:price]
        {name: name, description: description, price: price}
    end

    def map_products
        products = []
        Product.all.each do |product|
            dup_product = map_product product
            products << dup_product
        end
        products.to_json
    end

    def map_product product
        dup_product = {}
        dup_product[:id] = product.id
        dup_product[:name] = product.name
        dup_product[:description] = product.description
        dup_product[:price] = product.price.to_f
        dup_product
    end

    def add_to_cart
        p get_params params
        RestClient::Request.execute(:method => :post, :url => "http://localhost:4000/api/v1/item",
                            :payload => get_params(params),
                            :timeout => 10, :open_timeout => 10) do |response, request, result|
            if response.code == 200
                render json: response.body, status: 200
            else
                render json: response.body, status: 500
            end
        end
    end

    def get_params params
        param = {}
        param[:id] = params[:id]
        param[:name] = params[:name]
        param[:description] = params[:description]
        param[:price] = params[:price]
        param
    end
end
